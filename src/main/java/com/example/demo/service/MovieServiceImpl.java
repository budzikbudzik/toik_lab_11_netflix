package com.example.demo.service;

import com.example.demo.dto.MovieListDto;
import com.example.demo.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    MovieRepository movieRepository;

    public MovieListDto createGet(){
        return movieRepository.getMovies();
    }
}
