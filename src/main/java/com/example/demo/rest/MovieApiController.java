package com.example.demo.rest;

import com.example.demo.dto.MovieListDto;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MovieApiController {

    @Autowired
    private MovieService movieService;

    @CrossOrigin
    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        return ResponseEntity.ok().body(movieService.createGet());
    }
}
