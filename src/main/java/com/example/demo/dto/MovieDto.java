package com.example.demo.dto;

public class MovieDto {

    private Integer movieId;
    private String title;
    private Integer year;
    private String image;

    public MovieDto(Integer movieId, String title, Integer year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "MovieDto{" +
                "movieId='" + movieId + '\'' +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

}
